import standardShipsSet from "../utils/standardShipsSet";

export const initialState = {
        enemyField: [],
        enemyShips: [...standardShipsSet],
        areEnemyShipsInvisible: true,
        gameOver: false,
        logs: ['Ожидание хода...'],
};
