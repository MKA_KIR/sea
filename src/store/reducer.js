import {initialState} from "./initialState";


export const reducer = (state = initialState, action)=> {
    switch(action.type) {
        case "LOGIN":
            return {...state, profileData: action.payload};
        case "UPDATE_BALANCE":
            const newBalance = state.userBalance+action.payload
            return {...state, userBalance: newBalance}
        default:
            return state;
    }
};
