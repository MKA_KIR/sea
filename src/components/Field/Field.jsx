import React from 'react';
import FieldRow from '../FieldRow/FieldRow';

import './Field.scss'

const Field = (props) => {
    const { fieldMap } = props;
    return (
        <div className={`field ${props.whose ? props.whose : ""}`}>
            {fieldMap.map((row, index) => {
                return (
                    <FieldRow
                    key={index}
                    row={row}
                    onClick={(y, x) => props.onClick(y, x)}
                    />
                )
            })}
        </div>
    )
}

export default Field
