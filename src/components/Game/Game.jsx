import React, {useState} from 'react';
import Field from '../Field/Field';
import GameLog from '../GameLog/GameLog';
import placeShip from '../../utils/placeShip';


const Game = () => {
    const [enemyField, setEnemyField] = useState();
    const [enemyShips, setEnemyShips] = useState();
    const [gameOver, setGameOver] = useState();

    // первоначальное (пустое) состояние поля
    for (let i = 0; i < 10; i++) {
        enemyField.push([]);
    }

    for (let i = 0; i < 10; i++) {
        for (let j = 0; j < 10; j++) {
            enemyField[i].push({
                x: j,
                y: i,
                containsShip: false,
                shot: false,
                isShipVisible: false,
                shipId: null,
            });
        }
    }

    // расставляем стандартный набор кораблей
    enemyShips.forEach(ship => {
        placeShip(enemyField, ship)
    });
}

 const handleClick = (y, x) => {

     let enemyField;
     if (enemyField[y][x].shot) {
        return
    }

     let gameOver;
     if (gameOver) {
        return
    }

    this.setState(state => {
        const newField = [...state.enemyField];
        newField[y][x].shot = true;
        newField[y][x].isShipVisible = true;

        const newShips = [...state.enemyShips];
        const newLogs = [...state.logs];
        let gameOver = false;

        if (newField[y][x].containsShip) {
            const hittedShip = newShips.find(ship => (ship.id === newField[y][x].shipId));
            hittedShip.hitpoints--;
            // если хитпоинтов больше нуля, то это обычное попадание
            // если хитпоинтов ноль, то корабль уничтожен
            if (hittedShip.hitpoints > 0) {
                newLogs.push('Попадание!');
            } else {
                newLogs.push('Корабль уничтожен!');
            }

            if (newShips.every(ship => (ship.hitpoints === 0))) {
                newLogs.push('Игра окончена.');
                gameOver = true;
            }
        } else {
            newLogs.push('Мимо!');
        }

        return {
            enemyField: newField,
            enemyShips: newShips,
            gameOver: gameOver,
            logs: newLogs,
        }
    })
     return (
         <div className="game">
             {/* <Field whose="player" fieldMap={this.state.playerField} /> */}
             <Field
                 whose="enemy"
                 fieldMap={this.state.enemyField}
                 onClick={(y, x) => handleClick(y, x)}
             />
             <GameLog logs={this.state.logs}/>
         </div>
     );
}

export default Game;
